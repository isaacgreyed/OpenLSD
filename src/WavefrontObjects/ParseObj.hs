{-|
Module      : ParseObj
Description : Parses Wavefront .obj files
Copyright   : (c) Isaac Edelhauser, 2018
                  helmet put your name here if you want, 2018
License     : GPL-3
Maintainer  : isaacgreyed@gmail.com
Stability   : experimental
Portability : POSIX

In progress module to wavefront obj files into a list of points that OpenGL can render easily.
Made because no suitable alternative was found in haskell.
Currently supports:
  Importing from file
  Rigid bodies (not freeform objects) of an arbitrary size
Does not currently support:
  Freeform objects, curves
  Textures
  Normals
-}
module WavefrontObjects.ParseObj
where
import Graphics.Rendering.OpenGL
import WavefrontObjects.Vertexs
import WavefrontObjects.Faces
import WavefrontObjects.Normals
----Set of functions to render .obj files in OpenGL

-- | Given a filepath of a .obj file, it renders the file in the OpenGL context it is called.
renderObj :: String -> IO ()
renderObj f = do
  s <- file2strings f
  renderFaces (composeObjData (getVertexs s) (getFaces s)) (composeObjData (getVNormals s) (getFNormals (filterFaces s)))

loadObj :: [String] -> ([Face'],[Face'])
loadObj s = (composeObjData (getVertexs s) (getFaces s), composeObjData (getVNormals s) (getFNormals (filterFaces s)))

-- |Describes a face as a list of verticies
type Face' = [Vertex3']

-- |Renders a list of faces of an arbitrary length into the openGL context it is called in.
renderFaces :: [Face'] -> [Face'] -> IO ()
renderFaces [] ns = putStr ""
renderFaces xs ns = do
  renderFace (head xs) (head ns)
  renderFaces (tail xs) (tail ns)

-- | Renders an individual face into the openGL context it is called in.
renderFace :: Face' -> Face' ->IO ()
renderFace xs ns = renderPrimitive Polygon $ do
    let (a,b,c) = head ns
    normal (Normal3 a b c)
    let (x,y,z) = head xs
    vertex (Vertex3 x y z)
    let (a,b,c) = ns !! 1
    normal (Normal3 a b c)
    let (x,y,z) = xs!!1
    vertex (Vertex3 x y z)
    let (a,b,c) = ns !! 2
    normal (Normal3 a b c)
    let (x,y,z) = xs!!2
    vertex (Vertex3 x y z)
    if length xs >= 4 then do
      let (a,b,c) = ns !! 3
      normal (Normal3 a b c)
      let (x,y,z) = xs!!3
      vertex (Vertex3 x y z)
                      else putStr ""



renderWireframe :: String -> IO ()
renderWireframe f = do
  s <- file2strings f
  renderFrames (composeObjData (getVertexs s) (getFaces s))

renderFrames :: [Face'] -> IO ()
renderFrames [] = putStr ""
renderFrames xs = do
  renderFrame (head xs)
  renderFrames (tail xs)

renderFrame :: Face' -> IO ()
renderFrame xs = renderPrimitive LineLoop $
    mapM_ (\(x, y, z) -> vertex $ Vertex3 x y z) xs

-- | Given a list of verticies and unprocessed faces creates a renderable list of faces.
composeObjData :: [Vertex3'] -> [ObjFace] -> [Face']
composeObjData vs [] = []
composeObjData vs fs = data2face vs (head fs)
  :composeObjData vs (tail fs)

-- | Given a list of verticies and an unprocessed face creates a renderable face.
data2face :: [Vertex3'] -> ObjFace -> Face'
data2face vs [] = []
data2face vs fs = vs!!(head fs - 1):data2face vs (tail fs)

-- |Given a filepath in a string will return a list of strings seperated into lines.
file2strings :: String -> IO [String]
file2strings f = do
  s <- readFile f
  return (lines s)
