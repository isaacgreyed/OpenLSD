module WavefrontObjects.Normals
where
import WavefrontObjects.Vertexs (strings2vertexs, Vertex3')
import WavefrontObjects.Faces (strings2faces, ObjFace, vertex2int)


getVNormals :: [String] -> [Vertex3']
getVNormals f = strings2vertexs (filterNormal f)
-- | Filters from obj files seperated by lines all lines starting with vn
filterNormal :: [String] -> [String]
filterNormal f = [z | z <-f, z /= [], head z == 'v', z!!1 == 'n']

getFNormals :: [String] -> [ObjFace]
getFNormals [] = []
getFNormals xs = do
  let z = words (head xs)
  string2FNormal z : getFNormals (tail xs)

string2FNormal :: [String] -> ObjFace
string2FNormal [] = []
string2FNormal xs = if head (head xs) == 'f' then string2FNormal (tail xs)
  else (if removeFirstInt (head xs) !! 1 == '/' then getSecondInt else getThirdInt) (head xs):string2FNormal (tail xs)

getSecondInt :: String -> Int
getSecondInt xs = read (vertex2int (removeSlashes (removeFirstInt xs)))

getThirdInt :: String -> Int
getThirdInt xs = read (vertex2int (removeSlashes (removeFirstInt (removeSlashes (removeFirstInt xs)))))

removeFirstInt :: String -> String
removeFirstInt [] = []
removeFirstInt xs = if head xs /= '/' then removeFirstInt (tail xs) else xs

removeSlashes :: String -> String
removeSlashes xs
          | (xs !! 1) == '/' = snd (splitAt 2 xs)
          | head xs == '/' = tail xs
          | otherwise = xs
