module WavefrontObjects.Vertexs
    ( getVertexs,
      Vertex3',
      strings2vertexs
    ) where
import Graphics.Rendering.OpenGL

-- |Describes a Vertex with an x, y, and z for rendering
type Vertex3' = (GLdouble,GLdouble,GLdouble)

-- |Gets list of verticies given obj file seperated by lines
getVertexs :: [String] -> [Vertex3']
getVertexs f = strings2vertexs (filterVertex f)

{- |Given a list of strings returns only those that begin with v and are not followed by t or n.
    Used to filter out verticies from .obj files.
-}
filterVertex :: [String] -> [String]
filterVertex f = [z | z <-f,  z /= [], head z == 'v', z!!1 /= 'n', z!!1 /= 't']

{- |Given a list of strings in vertex format from .obj file
    it will return them as a list of verticies.
-}
strings2vertexs :: [String] -> [Vertex3']
strings2vertexs [] = []
strings2vertexs xs = do
  let z = words (head xs)
  (read (z !! 1), read (z !! 2), read (z !! 3)) :
    strings2vertexs (tail xs)
