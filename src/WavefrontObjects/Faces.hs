module WavefrontObjects.Faces
  ( getFaces,
    ObjFace,
    strings2faces,
    vertex2int,
    filterFaces
  ) where
import Graphics.Rendering.OpenGL

{- |Describes unprocessed faces from the .obj file as a list of integers,
    which will later be converted into a list of those verticies.
-}
type ObjFace = [Int]
-- |Gets list of unprocesed faces given obj file seperated by lines
getFaces :: [String] -> [ObjFace]
getFaces f = strings2faces (filterFaces f)

{- |Given a list of strings returns only those that begin with f.
    Used to filter out verticies from .obj files.
-}
filterFaces :: [String] -> [String]
filterFaces f = [z | z <-f, z /= [], head z == 'f']

{-|Given a list of strings in face format from .obj
   file it will return them as a list of faces.
-}
strings2faces :: [String] -> [ObjFace]
strings2faces [] = []
strings2faces xs = do
  let z = words (head xs)
  string2face z : strings2faces (tail xs)
-- |Given a list of strings in individual faces it will return one ObjFace
string2face :: [String] -> ObjFace
string2face [] = []
string2face xs = if head (head xs) == 'f' then string2face (tail xs)
  else read (vertex2int (head xs)):string2face (tail xs)
-- | Converts string from filtered ObjFace into a readable integer
vertex2int :: String -> String
vertex2int [] = []
vertex2int xs = if head xs `elem` "0123456789" then head xs : vertex2int (tail xs) else []
