module Shapes where

import Graphics.Rendering.OpenGL

rectangle :: GLdouble -> GLdouble -> IO ()
rectangle x y = renderPrimitive Quads $ do
  vertex (Vertex2 (negate x) (negate y) :: Vertex2 GLdouble)
  vertex (Vertex2 x (negate y) :: Vertex2 GLdouble)
  vertex (Vertex2 x y :: Vertex2 GLdouble)
  vertex (Vertex2 (negate x) y :: Vertex2 GLdouble)

square :: GLdouble -> IO ()
square x = rectangle x x
