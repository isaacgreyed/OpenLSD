module Main where

import Graphics.Rendering.OpenGL
import qualified Graphics.UI.GLFW as G
import System.Exit

import MaybeUtils (bool, maybe', unless')
import Callbacks  (errorCallback, keyCallback)
import Shapes
import WavefrontObjects.ParseObj
import GHC.Float
import WavefrontObjects.Vertexs
import WavefrontObjects.Faces
import WavefrontObjects.Normals
import Data.Time.Clock


toRadians :: Double -> Double
toRadians ang = (ang * pi) / 180

main :: IO ()
main = do
  G.setErrorCallback (Just errorCallback)
  successfulInit <- G.init
  -- if init fails then a pattern match error occurs and we exit program
  bool successfulInit exitFailure $ do
      mw <- G.createWindow 1080 720 "OpenDream" Nothing Nothing
      maybe' mw (G.terminate >> exitFailure) $ \window -> do
          G.makeContextCurrent mw
          G.setKeyCallback window (Just keyCallback)
          depthFunc $= Just Less
          lighting $= Enabled
          position (Light 0) $= Vertex4 0 0 (-1) 1
          ambient (Light 0) $= Color4 1 1 1 1
          diffuse (Light 0) $= Color4 1 1 1 1
          light (Light 0) $= Enabled
          s <- file2strings "palm_tree.obj"
          let obj = (composeObjData (getVertexs s) (getFaces s), composeObjData (getVNormals s) (getFNormals (filterFaces s)))
          print obj
          time <- getCurrentTime
          mainLoop window 0 (-1) 0 obj time
          G.destroyWindow window
          G.terminate
          exitSuccess

mainLoop :: G.Window -> GLdouble -> GLdouble -> GLdouble -> ([Face'],[Face']) -> UTCTime -> IO ()
mainLoop win px pz py obj time = unless' (G.windowShouldClose win) $ do
    ---Reshapes and clears window
    (width, height) <- G.getFramebufferSize win
    let ratio = fromIntegral width / fromIntegral height
    G.setCursorInputMode win G.CursorInputMode'Disabled
    viewport $= (Position 0 0, Size (fromIntegral width) (fromIntegral height))
    clear [ColorBuffer, DepthBuffer]
    --clear [ColorBuffer]
    --- Handling User Input
    --Mouse position to rads
    (x,y) <- G.getCursorPos win
    let y'
          | y / 300 > 3 = 3
          | y / 300 < 0.1 = 0.1
          | otherwise = y / 300
    if y > (300*3) then G.setCursorPos win x (300*3) else putStr ""
    if y <= (300*0.1) then G.setCursorPos win x (300*0.1) else putStr ""
    -- Detects keyboard input
    keya <- G.getKey win G.Key'A
    keyd <- G.getKey win G.Key'D
    keyw <- G.getKey win G.Key'W
    keys <- G.getKey win G.Key'S
    keyr <- G.getKey win G.Key'R
    keyf <- G.getKey win G.Key'F
    let pyf = (if keyr == G.KeyState'Pressed then 0.01 else 0) +
              (if keyf == G.KeyState'Pressed then (-0.01) else 0) + py
    -- Turns angle into z and x coords
    let lookz = (sin (x/300) * sin y')+pz
    let lookx = (cos (x/300) * sin y')+px
    let looky = cos y' + pyf
    -- Uses keyboard input to compute current velocity
    let spd = (if keyw == G.KeyState'Pressed then 0.01 else 0) +
              (if keys == G.KeyState'Pressed then (-0.01) else 0)
    let spdstrafe = (if keya == G.KeyState'Pressed then 0.01 else 0) +
                      (if keyd == G.KeyState'Pressed then (-0.01)  else 0)
    -- Turns velocity, angle, and current position into new position
    let pzf = pz + (sin (x/300)*spd) + (sin(x/300-(pi/2))*spdstrafe)
    let pxf = px + (cos (x/300)*spd) + (cos(x/300-(pi/2))*spdstrafe)
    --- Camera Projections
    matrixMode $= Projection
    loadIdentity
    -- Data for frustum projection displayed in let for readability
    let near = 0.001
        far  = 40
        fov  = 70
        ang  = (fov*pi)/360
        top  = near / (cos ang / sin ang)
        right = top*ratio
    frustum (negate right) right (negate top) top near far
    -- Determines position of camera, where the camera is looking, and where up is for the camera in that order
    lookAt (Vertex3 pxf pyf pzf) (Vertex3 lookx looky lookz) (Vector3 0 1 0)
    matrixMode $= Modelview 0
    position (Light 0) $= Vertex4 (double2Float pxf) (double2Float pyf) (double2Float pzf) 1
    loadIdentity
    ---Area to draw things to the screen
    -- Basic Colored Triangle for testing
    renderPrimitive Polygon $ do
        color  (Color3 1 1 1 :: Color3 GLdouble)
        vertex (Vertex3 (negate 0.6) (negate 0.4) (-2) :: Vertex3 GLdouble)
        vertex (Vertex3 0.6 (negate 0.4) (-2) :: Vertex3 GLdouble)
        vertex (Vertex3 0 0.6 (-2) :: Vertex3 GLdouble)
    renderPrimitive Triangles $ do
        color  (Color3 1 0 0 :: Color3 GLdouble)
        vertex (Vertex3 (negate 0.6) (negate 0.4) 0 :: Vertex3 GLdouble)
        vertex (Vertex3 0.6 (negate 0.4) 0 :: Vertex3 GLdouble)
        vertex (Vertex3 0 0.6 0 :: Vertex3 GLdouble)
    renderPrimitive Triangles $ do
        color  (Color3 0 1 0 :: Color3 GLdouble)
        vertex (Vertex3 1 (negate 0.4) ((-1)- 0.6) :: Vertex3 GLdouble)
        vertex (Vertex3 1 (negate 0.4) ((-1)+0.6) :: Vertex3 GLdouble)
        vertex (Vertex3 1 0.6 (-1) :: Vertex3 GLdouble)
    renderPrimitive Triangles $ do
        color  (Color3 0 0 1 :: Color3 GLdouble)
        vertex (Vertex3 (negate 1) (negate 0.4) ((-1)- 0.6) :: Vertex3 GLdouble)
        vertex (Vertex3 (negate 1) (negate 0.4) ((-1)+0.6) :: Vertex3 GLdouble)
        vertex (Vertex3 (negate 1) 0.6 (-1) :: Vertex3 GLdouble)
    loadIdentity
    --color (Color3 0 0 0 :: Color3 GLdouble)
    --renderWireframe "icosphere.obj"
    loadIdentity
    color (Color3 1 0 1 :: Color3 GLdouble)
    print "Obj loaded"
    let fs = obj
    uncurry renderFaces fs
    --Gets and prints FPS
    newtime <- getCurrentTime
    print (1 / diffUTCTime newtime time)
    --renderObj "Room.obj"
    --Call mainLoop again with updated variables
    G.swapBuffers win
    G.pollEvents
    mainLoop win pxf pzf pyf obj newtime
